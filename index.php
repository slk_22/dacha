<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");

$APPLICATION->SetPageProperty("description", "Дачный ежедневник - онлайн журнал для нового поколения садоводов и дачников. Животрепещущие темы!");
$APPLICATION->SetPageProperty("keywords", "дача, сад, посадки, семена, выращивать, здоровье, похудение");
$APPLICATION->SetPageProperty("title", "Дачный ежедневник - посадки вовремя");
$APPLICATION->SetPageProperty("NOT_SHOW_NAV_CHAIN", "Y");
$APPLICATION->SetTitle("Посадки вовремя");

global $arElementsFilterID;
$arElementsFilterID = array();

global $arSectionsFilterCode;
$arSectionsFilterCode = array();
?>
<div id="news_last" class="col_x2_left">
	<table>
		<tr>
			<td width="50%">
				<a href="/articles/garden/" style="text-decoration:none;">
					<h2 style="border-top: 2px solid #000; margin-bottom: 10px;padding: 3px 0;font-size:22px; margin:2px; border-bottom: 1px solid #CCC;">В саду</h2>
				</a>
				<?$arElementsID=$APPLICATION->IncludeComponent("bitrix:news.list", "day", array(
					"IBLOCK_TYPE" => "articles",
					"IBLOCK_ID" => "1",
					"NEWS_COUNT" => "2",
					"DISPLAY_DISPLAY" => "1",
					"SORT_BY1" => "propertysort_ITEM_STATUS",
					"SORT_ORDER1" => "ASC",
					"SORT_BY2" => "ACTIVE_FROM",
					"SORT_ORDER2" => "DESC",
					"FILTER_NAME" => "",
					"FIELD_CODE" => array(
						0 => "",
						1 => "",
					),
					"PROPERTY_CODE" => array(
						0 => "",
						1 => "",
					),
					"CHECK_DATES" => "Y",
					"DETAIL_URL" => "/articles/#SECTION_CODE#/#ELEMENT_CODE#/",
					"AJAX_MODE" => "N",
					"AJAX_OPTION_JUMP" => "N",
					"AJAX_OPTION_STYLE" => "Y",
					"AJAX_OPTION_HISTORY" => "N",
					"CACHE_TYPE" => "A",
					"CACHE_TIME" => "36000000",
					"CACHE_FILTER" => "N",
					"CACHE_GROUPS" => "Y",
					"PREVIEW_TRUNCATE_LEN" => "",
					"ACTIVE_DATE_FORMAT" => "d.m.Y",
					"SET_TITLE" => "N",
					"SET_STATUS_404" => "N",
					"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
					"ADD_SECTIONS_CHAIN" => "N",
					"HIDE_LINK_WHEN_NO_DETAIL" => "N",
					"PARENT_SECTION" => "5",
					"PARENT_SECTION_CODE" => "",
					"INCLUDE_SUBSECTIONS" => "Y",
					"DISPLAY_TOP_PAGER" => "N",
					"DISPLAY_BOTTOM_PAGER" => "N",
					"PAGER_TITLE" => "Новости",
					"PAGER_SHOW_ALWAYS" => "N",
					"PAGER_TEMPLATE" => "",
					"PAGER_DESC_NUMBERING" => "N",
					"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
					"PAGER_SHOW_ALL" => "N",
					"DISPLAY_DATE" => "N",
					"DISPLAY_NAME" => "Y",
					"DISPLAY_PICTURE" => "Y",
					"DISPLAY_PREVIEW_TEXT" => "Y",
					"DISPLAY_SECTION_NAME" => "N",
					"AJAX_OPTION_ADDITIONAL" => ""
					),
					false
				);?>
			</td>
			<td>
				<a href="/articles/business+in+the+country/" style="text-decoration:none;"><h2 style="border-top: 2px solid #000; margin-bottom: 10px; padding: 3px 0;font-size:22px; margin:2px; border-bottom: 1px solid #CCC;">На этой неделе</h2></a>

				<?$arElementsID=$APPLICATION->IncludeComponent("bitrix:news.list", "day", array(
						"IBLOCK_TYPE" => "articles",
						"IBLOCK_ID" => "1",
						"NEWS_COUNT" => "4",
						"DISPLAY_DISPLAY" => "0",
						"SORT_BY1" => "propertysort_ITEM_STATUS",
						"SORT_ORDER1" => "ASC",
						"SORT_BY2" => "ACTIVE_FROM",
						"SORT_ORDER2" => "DESC",
						"FILTER_NAME" => "",
						"FIELD_CODE" => array(
							0 => "",
							1 => "",
						),
						"PROPERTY_CODE" => array(
							0 => "",
							1 => "",
						),
						"CHECK_DATES" => "Y",
						"DETAIL_URL" => "/articles/#SECTION_CODE#/#ELEMENT_CODE#/",
						"AJAX_MODE" => "N",
						"AJAX_OPTION_JUMP" => "N",
						"AJAX_OPTION_STYLE" => "Y",
						"AJAX_OPTION_HISTORY" => "N",
						"CACHE_TYPE" => "A",
						"CACHE_TIME" => "36000000",
						"CACHE_FILTER" => "N",
						"CACHE_GROUPS" => "Y",
						"PREVIEW_TRUNCATE_LEN" => "",
						"ACTIVE_DATE_FORMAT" => "d.m.Y",
						"SET_TITLE" => "N",
						"SET_STATUS_404" => "N",
						"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
						"ADD_SECTIONS_CHAIN" => "N",
						"HIDE_LINK_WHEN_NO_DETAIL" => "N",
						"PARENT_SECTION" => "1102",
						"PARENT_SECTION_CODE" => "",
						"INCLUDE_SUBSECTIONS" => "Y",
						"DISPLAY_TOP_PAGER" => "N",
						"DISPLAY_BOTTOM_PAGER" => "N",
						"PAGER_TITLE" => "Новости",
						"PAGER_SHOW_ALWAYS" => "N",
						"PAGER_TEMPLATE" => "",
						"PAGER_DESC_NUMBERING" => "N",
						"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
						"PAGER_SHOW_ALL" => "N",
						"DISPLAY_DATE" => "N",
						"DISPLAY_NAME" => "Y",
						"DISPLAY_PICTURE" => "Y",
						"DISPLAY_PREVIEW_TEXT" => "Y",
						"DISPLAY_SECTION_NAME" => "N",
						"AJAX_OPTION_ADDITIONAL" => ""
					),
					false
				);?>
			</td>
		</tr>
	</table>

	<?$APPLICATION->IncludeComponent(
	"bitrix:catalog.section.list",
	"i",
	Array(
		"IBLOCK_TYPE" => "articles",
		"IBLOCK_ID" => "1",
		"SECTION_ID" => "",
		"SECTION_CODE" => "",
		"NEWS_COUNT" => "5",
		"FILTER_SECTION_CODE" => array(0=>"garden",),
		"FILTER_SECTION_garden_TEMPLATE" => "vert2",
		"FILTER_SECTION_garden_DISPLAY" => "5",
		"FILTER_SECTION_garden_STRING" => "5",
		"COUNT_ELEMENTS" => "N",
		"TOP_DEPTH" => "3",
		"SECTION_FIELDS" => array(0=>"",1=>"",),
		"SECTION_USER_FIELDS" => array(0=>"",1=>"",),
		"SORT_BY1" => "propertysort_ITEM_STATUS",
		"SORT_ORDER1" => "ASC",
		"SORT_BY2" => "ACTIVE_FROM",
		"SORT_ORDER2" => "DESC",
		"FILTER_NAME" => "arrFilter",
		"FIELD_CODE" => array(0=>"",1=>"",),
		"PROPERTY_CODE" => array(0=>"",1=>"",),
		"CHECK_DATES" => "Y",
		"SECTION_URL" => "/articles/#SECTION_CODE#/",
		"DETAIL_URL" => "/articles/#SECTION_CODE#/#ELEMENT_CODE#/",
		"CACHE_TYPE" => "A",
		"CACHE_TIME" => "36000000",
		"CACHE_GROUPS" => "Y",
		"ADD_SECTIONS_CHAIN" => "N",
		"HIDE_LINK_WHEN_NO_DETAIL" => "N",
		"DISPLAY_DATE" => "N",
		"DISPLAY_NAME" => "Y",
		"DISPLAY_PICTURE" => "Y",
		"DISPLAY_PREVIEW_TEXT" => "N",
		"DISPLAY_DISPLAY" => "4",
	  "ELEMENT_SORT_FIELD" => "name",
	  "ELEMENT_SORT_ORDER" => "asc",
	)
	);?>
	<div class="col wide-img clearfix">
		<div class="col_content col_left col_2">
			<?$APPLICATION->IncludeFile(
				$APPLICATION->GetTemplatePath("include_areas/banners/banner_index_left.php"),
				Array(),
				Array("MODE"=>"html")
			);
			?>
		</div>
		<div class="col_content col_right col_2 col_left_in">
			<?$APPLICATION->IncludeFile(
				$APPLICATION->GetTemplatePath("include_areas/banners/banner_index_right.php"),
				Array(),
				Array("MODE"=>"html")
			);
			?>
		</div>
	</div>
	<div class="col_left_in">
		 <?$APPLICATION->IncludeComponent(
			"bitrix:catalog.section.list",
			"i",
			Array(
				"IBLOCK_TYPE" => "articles",
				"IBLOCK_ID" => "1",
				"SECTION_ID" => "",
				"SECTION_CODE" => "",
				"NEWS_COUNT" => "5",
				"FILTER_SECTION_CODE" => array(0=>"flover",1=>"rose",2=>"repair",3=>"health",),
				"FILTER_SECTION_incidents_TEMPLATE" => "vert",
				"FILTER_SECTION_incidents_DISPLAY" => "2",
				"FILTER_SECTION_incidents_STRING" => "2",
				"FILTER_SECTION_stars_TEMPLATE" => "vert",
				"FILTER_SECTION_stars_DISPLAY" => "3",
				"FILTER_SECTION_stars_STRING" => "3",
				"FILTER_SECTION_science_TEMPLATE" => "hor",
				"FILTER_SECTION_science_DISPLAY" => "2",
				"FILTER_SECTION_science_STRING" => "2",
				"FILTER_SECTION_auto_TEMPLATE" => "vert",
				"FILTER_SECTION_auto_DISPLAY" => "4",
				"FILTER_SECTION_auto_STRING" => "2",
				"FILTER_SECTION_family_TEMPLATE" => "vert",
				"FILTER_SECTION_family_DISPLAY" => "3",
				"FILTER_SECTION_family_STRING" => "3",
				"FILTER_SECTION_health_TEMPLATE" => "vert",
				"FILTER_SECTION_health_DISPLAY" => "3",
				"FILTER_SECTION_health_STRING" => "3",
				"COUNT_ELEMENTS" => "Y",
				"TOP_DEPTH" => "1",
				"SECTION_FIELDS" => array(0=>"",1=>"",),
				"SECTION_USER_FIELDS" => array(0=>"",1=>"",),
				"SORT_BY1" => "propertysort_ITEM_STATUS",
				"SORT_ORDER1" => "ASC",
				"SORT_BY2" => "ACTIVE_FROM",
				"SORT_ORDER2" => "DESC",
				"FILTER_NAME" => "arrFilter",
				"FIELD_CODE" => array(0=>"",1=>"",),
				"PROPERTY_CODE" => array(0=>"",1=>"",),
				"CHECK_DATES" => "Y",
				"SECTION_URL" => "/articles/#SECTION_CODE#/",
				"DETAIL_URL" => "/articles/#SECTION_CODE#/#ELEMENT_CODE#/",
				"CACHE_TYPE" => "A",
				"CACHE_TIME" => "36000000",
				"CACHE_GROUPS" => "Y",
				"ADD_SECTIONS_CHAIN" => "N",
				"HIDE_LINK_WHEN_NO_DETAIL" => "N",
				"DISPLAY_DATE" => "N",
				"DISPLAY_NAME" => "Y",
				"DISPLAY_PICTURE" => "Y",
				"DISPLAY_PREVIEW_TEXT" => "N",
				"DISPLAY_DISPLAY" => "3",
				"BANNERS_INCLUDE" => "Y"
			)
		);?>
	</div>
<?// КОНЕЦ СРЕДНЕГО БЛОКА?>
</div>
<div class="col_x1_right">
	<div class="adv_block">
		 <? $APPLICATION->IncludeFile(
					$APPLICATION->GetTemplatePath("include_areas/adv_top.php"),
					Array(),
					Array("MODE"=>"html")
				);
			?>
	</div>
	<!-- Yandex.RTB R-A-207812-1 -->
	<div id="yandex_rtb_R-A-207812-1"></div>
	<script type="text/javascript">
		(function(w, d, n, s, t) {
			w[n] = w[n] || [];
			w[n].push(function() {
				Ya.Context.AdvManager.render({
					blockId: "R-A-207812-1",
					renderTo: "yandex_rtb_R-A-207812-1",
					horizontalAlign: false,
					async: true
				});
			});
			t = d.getElementsByTagName("script")[0];
			s = d.createElement("script");
			s.type = "text/javascript";
			s.src = "//an.yandex.ru/system/context.js";
			s.async = true;
			t.parentNode.insertBefore(s, t);
		})(this, this.document, "yandexContextAsyncCallbacks");
	</script>
	 <?

	// в огороде
	$arElementsFilterID = array_merge($arElementsFilterID, $arElementsID);
	$arrFilter = array("!ID" => $arElementsFilterID);

	$APPLICATION->IncludeComponent("bitrix:news.list", "vert", array(
	"IBLOCK_TYPE" => "articles",
	"IBLOCK_ID" => "1",
	"NEWS_COUNT" => "3",
	"DISPLAY_DISPLAY" => "3",
	"DISPLAY_LINE" => "1",
	"SORT_BY1" => "propertysort_ITEM_STATUS",
	"SORT_ORDER1" => "ASC",
	"SORT_BY2" => "ACTIVE_FROM",
	"SORT_ORDER2" => "DESC",
	"FILTER_NAME" => "arrFilter",
	"FIELD_CODE" => array(
		0 => "",
		1 => "",
	),
	"PROPERTY_CODE" => array(
		0 => "",
		1 => "",
	),
	"CHECK_DATES" => "Y",
	"DETAIL_URL" => "/articles/#SECTION_CODE#/#ELEMENT_CODE#/",
	"AJAX_MODE" => "N",
	"AJAX_OPTION_JUMP" => "N",
	"AJAX_OPTION_STYLE" => "Y",
	"AJAX_OPTION_HISTORY" => "N",
	"CACHE_TYPE" => "A",
	"CACHE_TIME" => "36000000",
	"CACHE_FILTER" => "N",
	"CACHE_GROUPS" => "Y",
	"PREVIEW_TRUNCATE_LEN" => "",
	"ACTIVE_DATE_FORMAT" => "d.m.Y",
	"SET_TITLE" => "N",
	"SET_STATUS_404" => "N",
	"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
	"ADD_SECTIONS_CHAIN" => "N",
	"HIDE_LINK_WHEN_NO_DETAIL" => "N",
	"PARENT_SECTION_CODE" => "vegeteble",
	"INCLUDE_SUBSECTIONS" => "Y",
	"DISPLAY_TOP_PAGER" => "N",
	"DISPLAY_BOTTOM_PAGER" => "N",
	"PAGER_TITLE" => "Новости",
	"PAGER_SHOW_ALWAYS" => "N",
	"PAGER_TEMPLATE" => "",
	"PAGER_DESC_NUMBERING" => "N",
	"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
	"PAGER_SHOW_ALL" => "N",
	"DISPLAY_DATE" => "N",
	"DISPLAY_NAME" => "Y",
	"DISPLAY_PICTURE" => "Y",
	"DISPLAY_PREVIEW_TEXT" => "N",
	"DISPLAY_SECTION_NAME" => "Y",
	"AJAX_OPTION_ADDITIONAL" => ""
	),
	false
);



// в огороде
	$arElementsFilterID = array_merge($arElementsFilterID, $arElementsID);
	$arrFilter = array("!ID" => $arElementsFilterID);

	$APPLICATION->IncludeComponent("bitrix:news.list", "vert", array(
	"IBLOCK_TYPE" => "articles",
	"IBLOCK_ID" => "1",
	"NEWS_COUNT" => "3",
	"DISPLAY_DISPLAY" => "3",
	"DISPLAY_LINE" => "1",
	"SORT_BY1" => "propertysort_ITEM_STATUS",
	"SORT_ORDER1" => "ASC",
	"SORT_BY2" => "ACTIVE_FROM",
	"SORT_ORDER2" => "DESC",
	"FILTER_NAME" => "arrFilter",
	"FIELD_CODE" => array(
		0 => "",
		1 => "",
	),
	"PROPERTY_CODE" => array(
		0 => "",
		1 => "",
	),
	"CHECK_DATES" => "Y",
	"DETAIL_URL" => "/articles/#SECTION_CODE#/#ELEMENT_CODE#/",
	"AJAX_MODE" => "N",
	"AJAX_OPTION_JUMP" => "N",
	"AJAX_OPTION_STYLE" => "Y",
	"AJAX_OPTION_HISTORY" => "N",
	"CACHE_TYPE" => "A",
	"CACHE_TIME" => "36000000",
	"CACHE_FILTER" => "N",
	"CACHE_GROUPS" => "Y",
	"PREVIEW_TRUNCATE_LEN" => "",
	"ACTIVE_DATE_FORMAT" => "d.m.Y",
	"SET_TITLE" => "N",
	"SET_STATUS_404" => "N",
	"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
	"ADD_SECTIONS_CHAIN" => "N",
	"HIDE_LINK_WHEN_NO_DETAIL" => "N",
	"PARENT_SECTION_CODE" => "expert",
	"INCLUDE_SUBSECTIONS" => "Y",
	"DISPLAY_TOP_PAGER" => "N",
	"DISPLAY_BOTTOM_PAGER" => "N",
	"PAGER_TITLE" => "Новости",
	"PAGER_SHOW_ALWAYS" => "N",
	"PAGER_TEMPLATE" => "",
	"PAGER_DESC_NUMBERING" => "N",
	"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
	"PAGER_SHOW_ALL" => "N",
	"DISPLAY_DATE" => "N",
	"DISPLAY_NAME" => "Y",
	"DISPLAY_PICTURE" => "Y",
	"DISPLAY_PREVIEW_TEXT" => "N",
	"DISPLAY_SECTION_NAME" => "Y",
	"AJAX_OPTION_ADDITIONAL" => ""
	),
	false
);
?>
</div>
 &nbsp;<br><?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>