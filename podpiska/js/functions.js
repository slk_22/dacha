﻿
function checkFields()
 {
  return_value = true;

  if ($("#mce-MERGE1").val() == "")
   {
//    $("#mce-MERGE1").val("Это обязательное поле");
    $("#mce-MERGE1").css("border-color", "#ce5753");
    return_value = false;
   }
  else
    $("#mce-MERGE1").css("border-color", "#5c799f");

  if ($("#mce-EMAIL").val() == "")
   {
//    $("#mce-EMAIL").val("Это обязательное поле");
    $("#mce-EMAIL").css("border-color", "#ce5753");
    return_value = false;
   }
  else
    $("#mce-EMAIL").css("border-color", "#5c799f");

  if ($("#merge_3").val() == ""  ||  $("#merge_3").val() == "Регион")
   {
//    $("#merge_3").val("Это обязательное поле");
    $("#merge_3").css("border-color", "#ce5753");
    return_value = false;
   }
  else
    $("#merge_3").css("border-color", "#5c799f");

  if (!($("#personal_data").is(":checked")))
   {
    $("#personal_data_label").css("color", "#ce5753");
    return_value = false;
   }
  else
    $("#personal_data_label").css("color", "#5c799f");

  if (!($("#subscribe_agree").is(":checked")))
   {
    $("#subscribe_agree_label").css("color", "#ce5753");
    return_value = false;
   }
  else
    $("#subscribe_agree_label").css("color", "#5c799f");

  if (!return_value)
   window.alert("Заполните обязательные поля!");
  
  return return_value;
 }