<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<? if(count($arResult["ITEMS"] > 0)): ?>
	<? foreach($arResult["ITEMS"] as $arItem): ?>
		<?
		$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
		$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
		?>
		<div id="<?=$this->GetEditAreaId($arItem['ID']);?>">
			<? if($arItem["DISPLAY_PROPERTIES"]["LINK"]["~VALUE"] != ''): ?>
				<a href="<?= $arItem["DISPLAY_PROPERTIES"]["LINK"]["~VALUE"]?>">
					<img src="<?=$arItem["IMG"]; ?>" alt="<?= $arItem["NAME"]; ?>">
				</a>
			<? else: ?>
				<img src="<?=$arItem["IMG"]; ?>" alt="<?= $arItem["NAME"]; ?>">
			<? endif; ?>
		</div>
	<? endforeach; ?>
<? endif; ?>
