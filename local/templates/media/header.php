<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
IncludeTemplateLangFile(__FILE__);
use Bitrix\Main\Page\Asset;

Asset::getInstance()->addCss($APPLICATION->GetTemplatePath("css/style.css"));
?>
<!DOCTYPE html>
<html>
<head>

	<title><?$APPLICATION->ShowTitle()?> | <?=COption::GetOptionString("main", "site_name")?></title>
	<meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0">

    <!--[if lt IE 9]>
		<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
		<script src="http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script>
	<![endif]-->
	<?$APPLICATION->ShowHead();?>
	<link href="<?=SITE_TEMPLATE_PATH?>/media.css?1" rel="stylesheet" />
	<link href="http://allfont.ru/allfont.css?fonts=freestyle-script-normal" rel="stylesheet" type="text/css" />
	<meta name='wmail-verification' content='6d07ceca240526afda23a85d7ec287be' />

	<?// TODO прописать нормально пути ?>
	<link rel="apple-touch-icon" sizes="180x180" href="/local/templates/media/favicon/apple-touch-icon.png">
	<link rel="icon" type="image/png" href="/local/templates/media/favicon/favicon-32x32.png" sizes="32x32">
	<link rel="icon" type="image/png" href="/local/templates/media/favicon/favicon-16x16.png" sizes="16x16">
	<link rel="manifest" href="/local/templates/media/favicon/manifest.json">
	<link rel="mask-icon" href="/local/templates/media/favicon/safari-pinned-tab.svg" color="#00b812">
	<link rel="shortcut icon" href="/local/templates/media/favicon/favicon.ico">
	<meta name="msapplication-config" content="/local/templates/media/favicon/browserconfig.xml">
	<meta name="theme-color" content="#ffffff">

	<meta name="verify-admitad" content="436381cb8f" />
</head>
<body>

<!-- Yandex.Metrika counter -->
<script type="text/javascript">
    (function (d, w, c) {
        (w[c] = w[c] || []).push(function() {
            try {
                w.yaCounter32354495 = new Ya.Metrika({
                    id:32354495,
                    clickmap:true,
                    trackLinks:true,
                    accurateTrackBounce:true,
                    webvisor:true
                });
            } catch(e) { }
        });

        var n = d.getElementsByTagName("script")[0],
            s = d.createElement("script"),
            f = function () { n.parentNode.insertBefore(s, n); };
        s.type = "text/javascript";
        s.async = true;
        s.src = "https://mc.yandex.ru/metrika/watch.js";

        if (w.opera == "[object Opera]") {
            d.addEventListener("DOMContentLoaded", f, false);
        } else { f(); }
    })(document, window, "yandex_metrika_callbacks");
</script>
<noscript><div><img src="https://mc.yandex.ru/watch/32354495" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->

<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
  ga('create', 'UA-75298033-1', 'auto');
  ga('send', 'pageview');

</script>

<?$APPLICATION->ShowPanel()?>

<div id="page">
	<header>
		<div class="wrap">
			<div class="wrapIn clearfix">
				<a class="logo" href="<?=SITE_DIR?>"><?
					$APPLICATION->IncludeFile(
						$APPLICATION->GetTemplatePath("include_areas/logo.php"),
						Array(),
						Array("MODE"=>"html")
					);
				?></a>
				<div class="logofon"><div class="logofontxt">Ваш дачный<br />помощник</div></div>
				<div class="topblock">
					<div class="topblocktxt">
						<span class="butt0o">
						<?$APPLICATION->IncludeComponent(
							"altasib:geobase.select.city",
							".default",
							Array(
							)
						);?>
						</span>
					</div>
					<div>
						<?$APPLICATION->IncludeComponent("bitrix:search.title", "i", array(
							"NUM_CATEGORIES" => "1",
							"TOP_COUNT" => "5",
							"ORDER" => "date",
							"USE_LANGUAGE_GUESS" => "N",
							"CHECK_DATES" => "N",
							"SHOW_OTHERS" => "N",
							"PAGE" => SITE_DIR."search/",
							"CATEGORY_0_TITLE" => "",
							"CATEGORY_0" => array(
								0 => "no",
							),
							"SHOW_INPUT" => "Y",
							"INPUT_ID" => "title-search-input",
							"CONTAINER_ID" => "search_form",
							"SEARCH_TEXT_SAMPLE" => "Поиск по сайту"
							),
							false
						);?>
					</div>
					<br />
					<hr />
					<div class="topboxtxt">
						Получай свежие новости  <a href="/podpiska/index.html" class="butto1">Подпишись на рассылку</a>
					</div>
					<div class="buttimg">
						<a href="https://vk.com/saddaily" target="_blank"><img src="/arcticmodal/img/soc1.png" /></a><a href="https://www.facebook.com/%D0%94%D0%B0%D1%87%D0%BD%D1%8B%D0%B9-%D0%B5%D0%B6%D0%B5%D0%B4%D0%BD%D0%B5%D0%B2%D0%BD%D0%B8%D0%BA-125469574459206/" target="_blank"><img src="/arcticmodal/img/soc2.png" /></a><a href="http://ok.ru/dachnyyezh" target="_blank"><img src="/arcticmodal/img/soc3.png" /></a>
					</div><img class="topblockimg" src="/arcticmodal/img/soc4.png" />
				</div>
			</div>
		</div>
		<?$APPLICATION->IncludeComponent(
			"bitrix:menu",
			"head",
			Array(
				"ROOT_MENU_TYPE" => "top",
				"MAX_LEVEL" => "1",
				"CHILD_MENU_TYPE" => "top",
				"USE_EXT" => "Y",
				"DELAY" => "N",
				"ALLOW_MULTI_SELECT" => "N",
				"MENU_CACHE_TYPE" => "N",
				"MENU_CACHE_TIME" => "N",
				"MENU_CACHE_USE_GROUPS" => "Y",
				"MENU_CACHE_GET_VARS" => ""
			),
		false
		);?>
	</header>
</div>
<div id="content" class="wrap">
	<div class="wrapIn clearfix">

		<?/*широкий баннер */?>
		<div class="col wide-img">
			<?$APPLICATION->IncludeFile(
					$APPLICATION->GetTemplatePath("include_areas/banners/banner_wide.php"),
					Array(),
					Array("MODE"=>"html")
				);
			?>
		</div>

		<? if ($APPLICATION->GetCurDir()!=SITE_DIR): ?>
			<div class="col_x2_left">
				<div class="col_left_in_noborder">
		<? endif; ?>
		<? if ($APPLICATION->GetCurDir()!=SITE_DIR && strpos($APPLICATION->GetCurDir(),"articles/")===false): ?>
			<h2><?$APPLICATION->ShowTitle()?></h2>
		<?endif;?>