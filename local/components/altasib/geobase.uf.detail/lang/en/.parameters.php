<?
$MESS["ALTASIB_USERFIELD_LIST"] = "List of custom fields to display";
$MESS['ALTASIB_GEOBASE_DEMO_MODE'] = "The module is in demonstration mode. <a target='_blank' href='http://marketplace.1c-bitrix.ru/tobasket.php?ID=#MODULE#'>Buy version without limitation</a>";
$MESS['ALTASIB_GEOBASE_DEMO_EXPIRED'] = "The demonstration period of the module is ended. <a target='_blank' href='http://marketplace.1c-bitrix.ru/tobasket.php?ID=#MODULE#'>Buy unit</a>";
$MESS['ALTASIB_GEOBASE_NF'] = "Module #MODULE# not found";
$MESS['ALTASIB_GEOBASE_UF_SHOW_CITY'] = "Show city in the list fields";
$MESS['ALTASIB_GEOBASE_UF_SHOW_REGION'] = "Show the region in the list of fields";
$MESS['ALTASIB_GEOBASE_UF_SHOW_COUNTRY'] = "Show the country in the list of fields";
$MESS['ALTASIB_GEOBASE_UF_SHOW_COUNTRY_CODE'] = "Show the code of the country in the list of fields";
$MESS['SHOW_CITY_TIP'] = "Adding a field with the name of certain city in the list of output fields";
$MESS['SHOW_REGION_TIP'] = "Adding a field with the name of certain region in the list of output fields";
$MESS['SHOW_COUNTRY_TIP'] = "Adding a field with the name of certain country in the list of output fields";
$MESS['SHOW_COUNTRY_CODE_TIP'] = "Adding a field with the name of certain country code in the list of output fields";
$MESS['ALTASIB_GEOBASE_UF_NOTSHOW_NAME'] = "Do not display the names of the fields";
$MESS['NO_NAME_TIP'] = "Hides the names of all output fields";
$MESS['ALTASIB_GEOBASE_UF_SHOW_DEFAULT'] = "Show the default values for user-defined fields with a empty value";
$MESS['SHOW_DEFAULT_TIP'] = "The ability to display the default values set in the advanced settings of the custom field";
$MESS['ALTASIB_GEOBASE_UF_NOTSHOW_EMPTY'] = "Do not show empty field with the value";
$MESS['NO_EMPTY_TIP'] = "Hides fields that are empty";
?>