<?
$MESS ["ALTASIB_IS"] = "Магазин готовых решений для 1С-Битрикс";
$MESS ["ALTASIB_USERFIELD_LIST"] = "Список пользовательских полей для вывода";

$MESS['ALTASIB_GEOBASE_DEMO_MODE'] = "Модуль работает в демонстрационном режиме. <a target='_blank' href='http://marketplace.1c-bitrix.ru/tobasket.php?ID=#MODULE#'>Купить версию без ограничений</a>";
$MESS['ALTASIB_GEOBASE_DEMO_EXPIRED'] = "Демонстрационный период работы модуля закончился. <a target='_blank' href='http://marketplace.1c-bitrix.ru/tobasket.php?ID=#MODULE#'>Купить модуль</a>";
$MESS['ALTASIB_GEOBASE_NF'] = "Модуль #MODULE# не найден";
$MESS['ALTASIB_GEOBASE_UF_SHOW_CITY'] = "Показывать город в списке полей";
$MESS['ALTASIB_GEOBASE_UF_SHOW_REGION'] = "Показывать регион в списке полей";
$MESS['ALTASIB_GEOBASE_UF_SHOW_COUNTRY'] = "Показывать страну в списке полей";
$MESS['ALTASIB_GEOBASE_UF_SHOW_COUNTRY_CODE'] = "Показывать код страны в списке полей";
$MESS['SHOW_CITY_TIP'] = "Добавлять поле с названием определенного города в списке выводимых полей";
$MESS['SHOW_REGION_TIP'] = "Добавлять поле с названием определенного региона в списке выводимых полей";
$MESS['SHOW_COUNTRY_TIP'] = "Добавлять поле с названием определенной страны в списке выводимых полей";
$MESS['SHOW_COUNTRY_CODE_TIP'] = "Добавлять поле с названием определенного кода страны в списке выводимых полей";
$MESS['ALTASIB_GEOBASE_UF_NOTSHOW_NAME'] = "Не выводить названия полей";
$MESS['NO_NAME_TIP'] = "Скрываются названия всех выводимых полей";
$MESS['ALTASIB_GEOBASE_UF_SHOW_DEFAULT'] = "Выводить значения по умолчанию для пользовательских полей с пустым значением";
$MESS['SHOW_DEFAULT_TIP'] = "Возможность вывода значения по умолчанию, заданного в дополнительных настройках пользовательского поля";
$MESS['ALTASIB_GEOBASE_UF_NOTSHOW_EMPTY'] = "Не выводить поля с пустым значением";
$MESS['NO_EMPTY_TIP'] = "Скрываются поля, значения которых пустые";
?>